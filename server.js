const express = require('express')
const app = express()
const hbs = require('hbs')

app.use(express.static(__dirname + '/public'))
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'))


hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs')

app.get('/', (req, res) => {
  res.render('home', {
    title: 'Inicio'
  })
})

app.get('/myservices', (req, res) => {
  res.render('myservices', {
    title: 'Mis servicios'
  })
})

app.get('/portfolio', (req, res) => {
  res.render('portfolio', {
    title: 'Portafolio'
  })
})

app.get('/opinions', (req, res) => {
  res.render('opinions', {
    title: 'Opiniones'
  })
})



app.listen(process.env.PORT || 3000, () => {
  console.log('el servidor fue iniciado')
})